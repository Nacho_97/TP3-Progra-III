package datos;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import codigoDeNegocio.Instancia;
//import codigoDeNegocio.Jugador;
//import codigoDeNegocio.Pais;
//import codigoDeNegocio.Posicion;

public class Datos 
{
	private Instancia instancia;
	private String ruta = "src/datos/miInstancia.txt";
	
	public Datos()
	{
		this.instancia = this.cargarInstancia();
	}
	
	public Instancia dameInstancia()
	{
		return this.instancia;
	}
	
	public void guardarDatos(Instancia instancia)
	{
		try
		{
			ObjectOutputStream escribiendo_fichero = new ObjectOutputStream(new FileOutputStream(this.ruta));
			
			escribiendo_fichero.writeObject(instancia);
			
			escribiendo_fichero.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	private Instancia cargarInstancia() 
	{
		Instancia ret = null;
		try 
		{
			ObjectInputStream recuperando_datos = new ObjectInputStream(new FileInputStream(this.ruta));
			
			ret = (Instancia) recuperando_datos.readObject();
			
			recuperando_datos.close();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return ret;
	}
	
	//ESTE MAIN ES PARA HARDCODEAR LOS JUGADORES QUE SE QUIERAN AGREGAR EN LA PERMANENCIA DEL PROGRAMA
	/*public static void main(String[] args) 
	{
		Instancia instanciaAux = new Instancia();
		//ARQUEROS
		instanciaAux.agregarJugador(new Jugador("Franco Armani", 0, 1, 8.50,new Posicion[] {Posicion.arquero}, Pais.Argentina, 0));
		instanciaAux.agregarJugador(new Jugador("Manuel Neuer", 0, 1, 9,new Posicion[]{Posicion.arquero}, Pais.Alemania, 0));
		instanciaAux.agregarJugador(new Jugador("Gianluigi Buffon", 0, 0, 9.2,new Posicion[]{Posicion.arquero}, Pais.Italia, 0));
		instanciaAux.agregarJugador(new Jugador("Iker Casillas", 0, 2, 8.90,new Posicion[]{Posicion.arquero}, Pais.Espania, 0));	
		instanciaAux.agregarJugador(new Jugador("Ter Stegen", 0, 2, 8.95,new Posicion[]{Posicion.arquero}, Pais.Alemania, 0));	
		instanciaAux.agregarJugador(new Jugador("Thibaut Courtois", 0, 0, 8.20,new Posicion[]{Posicion.arquero}, Pais.Francia, 0));
		instanciaAux.agregarJugador(new Jugador("Keylor Navas", 0, 0, 9.50,new Posicion[]{Posicion.arquero}, Pais.CostaRica, 0));
		
		//DEFENSORES
		instanciaAux.agregarJugador(new Jugador("Nicolas Otamendi", 6, 2, 7.69,new Posicion[]{Posicion.centralDer,Posicion.centralIzq}, Pais.Argentina, 1));
		instanciaAux.agregarJugador(new Jugador("Jonas Hector", 3, 1, 7.83,new Posicion[]{Posicion.lateralIzq}, Pais.Alemania, 0));
		instanciaAux.agregarJugador(new Jugador("Kyle Walker", 2, 0, 7,new Posicion[]{Posicion.lateralDer}, Pais.Inglaterra, 1));
		instanciaAux.agregarJugador(new Jugador("Kim Young-gwon", 10, 3, 9.5,new Posicion[]{Posicion.centralIzq,Posicion.centralDer}, Pais.CoreaDelSur, 0));
		
		//VOLANTES
		//IZQUIERDOS
		instanciaAux.agregarJugador(new Jugador("Son Heung-min", 0, 0, 5,new Posicion[]{Posicion.volanteIzq}, Pais.CoreaDelSur, 2));
		instanciaAux.agregarJugador(new Jugador("Manuel Lanzini", 10, 4, 8.3,new Posicion[]{Posicion.volanteIzq}, Pais.Argentina, 2));
		instanciaAux.agregarJugador(new Jugador("Jose Paulinho", 5, 3, 8.9,new Posicion[]{Posicion.volanteIzq}, Pais.Brasil, 1));
		//CENTRALES
		instanciaAux.agregarJugador(new Jugador("Eric Dier", 0, 0, 8,new Posicion[]{Posicion.volanteCentral}, Pais.Inglaterra, 1));
		instanciaAux.agregarJugador(new Jugador("Javier Mascherano", 15, 6, 8,new Posicion[]{Posicion.volanteCentral}, Pais.Argentina, 2));
		instanciaAux.agregarJugador(new Jugador("Lucas Biglia", 8, 2, 7,new Posicion[]{Posicion.volanteCentral}, Pais.Argentina, 0));
		instanciaAux.agregarJugador(new Jugador("Luka Modric", 3, 1, 9.6,new Posicion[]{Posicion.volanteCentral,Posicion.volanteDer}, Pais.Croacia, 1));
		//DERECHOS
		instanciaAux.agregarJugador(new Jugador("Henderson", 0, 0, 6,new Posicion[]{Posicion.volanteDer}, Pais.Inglaterra, 2));
		instanciaAux.agregarJugador(new Jugador("Carlos Sanchez", 11, 3, 8,new Posicion[]{Posicion.volanteDer}, Pais.Uruguay, 2));
		instanciaAux.agregarJugador(new Jugador("Enzo Perez", 10, 4, 8.8,new Posicion[]{Posicion.volanteDer,Posicion.volanteIzq}, Pais.Argentina, 0));
		
		//PUNTEROS
		//IZQUIERDO
		instanciaAux.agregarJugador(new Jugador("Neymar Jr", 6, 4, 9.50,new Posicion[]{Posicion.punteroIzq}, Pais.Brasil, 8));
		instanciaAux.agregarJugador(new Jugador("Paulo Dybala", 1, 0, 8.50,new Posicion[]{Posicion.punteroIzq}, Pais.Argentina, 3));
		//CENTRAL
		instanciaAux.agregarJugador(new Jugador("Antoine Griezman", 5, 4, 9,new Posicion[]{Posicion.centroDelantero,Posicion.punteroIzq}, Pais.Francia, 5));
		instanciaAux.agregarJugador(new Jugador("Cristiano Ronaldo", 0, 0, 9,new Posicion[] {Posicion.centroDelantero}, Pais.Portugal, 7));
		//DERECHO
		instanciaAux.agregarJugador(new Jugador("Lionel Messi", 0, 0, 9,new Posicion[]{Posicion.punteroDer,Posicion.centroDelantero}, Pais.Argentina, 11));
		instanciaAux.agregarJugador(new Jugador("Timo Werner", 0, 0, 9,new Posicion[]{Posicion.punteroDer,Posicion.punteroIzq}, Pais.Alemania, 4));
		



		
		//(	NOMBRE  FALTAS  TARJETAS  PUNTAJE  POSICIONES  NACIONALIDAD  GOLES)
		try
		{
			ObjectOutputStream escribiendo_fichero = new ObjectOutputStream(new FileOutputStream("src/datos/miInstancia.txt"));
			
			escribiendo_fichero.writeObject(instanciaAux);
			
			escribiendo_fichero.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		System.out.println("Datos Cargados!");
	}*/

}
