package codigoDeNegocio;

import java.util.ArrayList;
import java.util.Collections;

public class Poblacion
{
	private Instancia instancia;
	private ArrayList<Individuo> individuos;
	
	private static int tamanoPoblacion = 1000; //predeterminado en 1000. pongo en 5 porque es alto overkill
	private static int iteraciones = 500; //predeterminado en 1000
	private static int mutadosEnCadaIteracion = 250;
	private static int recombinadosEnCadaIteracion = 250;
	private static int eliminadosEnCadaIteracion = 500;
	private Individuo mejor;
	
	private static Generador generador;
	
	public static void setGenerador(Generador nuevoGenerador)
	{
		generador = nuevoGenerador;
	}
	
	public Poblacion(Instancia instancia)
	{
		mejor = new Individuo(instancia,new Integer[]{0,0,0,0,0,0,0,0,0,0,0});
		individuos = new ArrayList<Individuo>(tamanoPoblacion);
		this.instancia = instancia;
		for(int i=0; i<tamanoPoblacion; ++i)
			individuos.add(new Individuo(instancia));
	}
	
	public Individuo simular() throws CloneNotSupportedException
	{
		for (int iteracion = 0; iteracion < iteraciones ; iteracion++)
		{
			mutarAlgunos();
			recombinarAlgunos();
			eliminarPeores();
			guardarMejor();
			agregarNuevos();
		}
		Individuo posibleOtro = getMejor();
		return (mejor.fitness()>=posibleOtro.fitness() ? mejor : posibleOtro);
	}

	private void guardarMejor() throws CloneNotSupportedException
	{
		if (mejor.fitness()< individuos.get(0).fitness())
		{
			mejor = (Individuo)individuos.get(0).clone();
		}
	}

	private void mutarAlgunos()
	{
		for (int i = 0; i < mutadosEnCadaIteracion; i++)
		{
			int aMutar = generador.nextInt(tamanoIndividuos());
			individuos.get(aMutar).mutar();
		}
	}
	
	private void recombinarAlgunos()
	{
		for(int i=0; i<recombinadosEnCadaIteracion; ++i)
		{
			int indice1 = generador.nextInt(tamanoIndividuos());
			int indice2 = generador.nextInt(tamanoIndividuos());
			
			Individuo padre1 = individuos.get(indice1);
			Individuo padre2 = individuos.get(indice2);
			for(Individuo hijo : padre1.recombinar(padre2))
				individuos.add(hijo);
		}
	}

	private void eliminarPeores() throws CloneNotSupportedException
	{
		Collections.sort(individuos);
		Collections.reverse(individuos);
		for(int i=0; i<eliminadosEnCadaIteracion; ++i)
			individuos.remove( individuos.size()-1 );
	}
	
	private void agregarNuevos()
	{
		while( individuos.size() < tamanoIndividuos() )
			individuos.add(new Individuo(instancia));
	}
	
	private int tamanoIndividuos()
	{
		return individuos.size();
	}
	
	public Individuo getMejor()
	{
		return Collections.max(individuos);
	}
	
}
