package codigoDeNegocio;

public class GeneradorTesting implements Generador
{

	private int[] aleatoriosADar;
	private int iterador;
	private int numeroFijo;
	
	public GeneradorTesting(int[] aleatorios,int bit)
	{
		aleatoriosADar = aleatorios;
		iterador = 0;
		numeroFijo = bit;
	}
	
	@Override
	public int nextInt(int i)
	{
		if (aleatoriosADar!=null)
			return aleatoriosADar[iterador++];
		else
			return numeroFijo;
	}
	

}
