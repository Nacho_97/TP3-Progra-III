package codigoDeNegocio;

import java.io.Serializable;

public enum Pais implements Serializable
{
	Alemania,
	ArabiaSaudi,
	Argentina,
	Australia,
	Austria,
	Brasil,
	Belgica,
	Colombia,
	CoreaDelSur,
	CostaRica,
	Croacia,
	Dinamarca,
	Egipto,
	Espania,
	Francia,
	Inglaterra,
	Iran,
	Islandia,
	Italia,
	Japon,
	Marruecos,
	Mexico,
	Nigeria,
	Peru,
	Polonia,
	Portugal,
	Rusia,
	Senegal,
	Serbia,
	Suecia,
	Suiza,
	Tunez,
	Uruguay
}