package codigoDeNegocio;

import java.io.Serializable;

public class Jugador implements Serializable
{
	private static final long serialVersionUID = 1L;
	private String nombre;
	private int faltas;
	private int tarjetas;
	private double puntajePromedio;
	private Posicion[] posiciones;
	private Pais nacionalidad;
	private int goles;
	
	public Jugador(String nombre, int faltas, int tarjetas, double puntajePromedio, Posicion[] posiciones, Pais nacionalidad, int goles) 
	{
		this.nombre = nombre;
		this.faltas = faltas;
		this.tarjetas = tarjetas;
		this.puntajePromedio = puntajePromedio;
		this.posiciones = posiciones;
		this.nacionalidad = nacionalidad;
		this.goles = goles;
	}

	public double getCoeficienteJugador()
	{
		return 2 * this.goles - (this.faltas/10) - this.tarjetas + puntajePromedio;
	}
	
	public String getNombre() 
	{
		return nombre;
	}

	public double getPuntajePromedio() 
	{
		return puntajePromedio;
	}

	public Posicion[] getPosiciones() 
	{
		return posiciones;
	}

	public void setFaltas(int faltas) 
	{
		this.faltas = faltas;
	}
	
	public int getFaltas()
	{
		return this.faltas;
	}

	public void setTarjetas(int tarjetas) 
	{
		this.tarjetas = tarjetas;
	}
	
	public Pais getNacionalidad()
	{
		return this.nacionalidad;
	}

	public int getCantidadTarjetas()
	{
		return this.tarjetas;
	}

	public int getCantidadDeGoles()
	{
		return this.goles;
	}	
}
