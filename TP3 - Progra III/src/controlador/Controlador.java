package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;

import codigoDeNegocio.Instancia;
import codigoDeNegocio.Jugador;
import datos.Datos;
import view.MainApp;

public class Controlador implements ActionListener
{
	private Instancia instancia;
	private Datos misDatos;
	private MainApp view;
	private Jugador buscado;
	
	public Controlador(MainApp v)
	{
		this.misDatos = new Datos();
		this.instancia = this.misDatos.dameInstancia();		
		this.view = v;
	}

	@Override
	public void actionPerformed(ActionEvent e) 
	{
		if(e.getActionCommand().equalsIgnoreCase("Generar Mejor Equipo"))
		{
			System.out.println("Generar Mejor Equipo");
			this.view.getVentanaEquipo().generarMejorEquipo(instancia);
		}
		if(e.getActionCommand().equalsIgnoreCase("Buscar"))
		{
			this.buscado = this.instancia.getJugador(this.view.getNombreABuscar());
			if (this.buscado == null)
			{
				JOptionPane.showMessageDialog(this.view, "No se encontro un jugador con ese nombre", "Jugador no encontrado", 1);
			}
			else
			{
				this.view.mostrarDatosDel(this.buscado);
				this.view.habilitarAgregarALista();
			}
		}
		if(e.getActionCommand().equalsIgnoreCase("Ejecutar"))
		{
			this.view.crearVentanaEquipo();
		}
		if(e.getActionCommand().equalsIgnoreCase("Agregar Jugador"))
		{
			this.view.crearVentanaJugador();
		}
		if(e.getActionCommand().equalsIgnoreCase("Agregar a la lista"))
		{
			this.view.agregarAListaPreseleccionado(this.buscado);
		}
		if(e.getActionCommand().equalsIgnoreCase("Agregar al equipo"))
		{
			String nombreJugador = this.view.getVentanaEquipo().getPanelLista().getJugadorSelected();
			if (nombreJugador.equals("Mi equipo"))
			{
				JOptionPane.showMessageDialog(this.view, "Seleccione un jugador.", "Mi equipo",1);
				return;
			}
			else
			{
				this.view.getVentanaEquipo().getPanelCancha().setJugador(this.view.getVentanaEquipo().getJugadorSeleccionado());
				this.view.getVentanaEquipo().actualizarVisualDeEquipo();
			}
		}
		if(e.getActionCommand().equalsIgnoreCase("Cargar jugador"))
		{
			view.crearVentanaCargaJugador();
		}
		if(e.getActionCommand().equalsIgnoreCase("Guardar"))
		{
			try
			{
				Jugador aux = this.view.dameJugadorAGuardar();
				this.instancia.agregarJugador(aux);
			}
			catch(IllegalArgumentException ex)
			{
				JOptionPane.showMessageDialog(view.getVentanaDeCarga(), ex.getMessage());
			}
			this.misDatos.guardarDatos(this.instancia);
			this.view.cerrarVentanaDeCarga();
		}
	}

	public Instancia getInstancia()
	{
		return this.instancia;
	}
}
