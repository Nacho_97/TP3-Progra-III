package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import codigoDeNegocio.Jugador;
import controlador.Controlador;

public class MainApp extends JFrame implements ActionListener
{
	private static final long serialVersionUID = 1L;
	private VentanaEquipo ventanaEquipo;
	private JButton btnEjecutar;
	private JButton btnCargarJugador;
	private Controlador controlador;
	private VentanaJugador ventanaJugador;
	private VentanaDeCarga ventanaCarga;

	public MainApp()
	{
		configurarVentana();
		configurarComponentes();
		this.controlador = new Controlador(this);
	}

	private void configurarVentana()
	{
		this.setBounds(100,100,250,400);
		this.setResizable(false);
		this.setTitle("Juego del mundial");
		this.setLayout(null);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setVisible(true);
	}
	
	private void configurarComponentes() 
	{
		this.btnEjecutar = new JButton("Ejecutar");
		this.btnEjecutar.addActionListener(this);
		this.btnEjecutar.setSize(150, 30);
		this.btnEjecutar.setLocation(((this.getBounds().width/4)*2)-(this.btnEjecutar.getSize().width/2),  ((this.getBounds().height/4)*2)-(this.btnEjecutar.getSize().height/2));
		this.getContentPane().add(btnEjecutar);
		
		this.btnCargarJugador = new JButton("Cargar jugador");
		this.btnCargarJugador.addActionListener(this);
		this.btnCargarJugador.setSize(150, 30);
		this.btnCargarJugador.setLocation(((this.getBounds().width/4)*2)-(this.btnCargarJugador.getSize().width/2),  this.btnEjecutar.getBounds().y+50);
		this.getContentPane().add(btnCargarJugador);
	}
	
	public static void main(String[] args) 
	{
		System.setProperty("java.util.Arrays.useLegacyMergeSort", "true");
		MainApp v = new MainApp();
		v.setVisible(true);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) 
	{
		this.controlador.actionPerformed(e);
		
	}

	public String getNombreABuscar()
	{
		return this.ventanaJugador.getJugadorABuscar();
	}

	public void agregarAListaPreseleccionado(Jugador jugador) 
	{
		try
		{
			this.ventanaEquipo.agregarJugadorALista(jugador);
			this.ventanaJugador.setVisible(false);
		}
		catch(IllegalArgumentException e)
		{
			JOptionPane.showMessageDialog(this.ventanaJugador, "El jugador ya esta en la lista", "Jugador Repetido", 1);
			return;
		}
	}

	public void crearVentanaEquipo()
	{
		this.ventanaEquipo = new VentanaEquipo(this);
		this.setVisible(false);
	}

	public void crearVentanaJugador() 
	{
		this.ventanaJugador = new VentanaJugador(this.ventanaEquipo);
		this.ventanaEquipo.setVentanaJugadorActive(true);
	}

	public void habilitarAgregarALista() 
	{
		this.ventanaJugador.setAgregarAListaActive(true);
	}

	public void mostrarDatosDel(Jugador jugador)
	{
		this.ventanaJugador.mostrarJugador(jugador);
	}
	
	public Controlador getControlador()
	{
		return this.controlador;
	}
	
	public VentanaEquipo getVentanaEquipo()
	{
		return this.ventanaEquipo;
	}

	public VentanaJugador getVentanaJugador()
	{
		return this.ventanaJugador;
	}

	public void crearVentanaCargaJugador() 
	{
		this.ventanaCarga = new VentanaDeCarga(this);
	}

	public Jugador dameJugadorAGuardar()
	{
		return this.ventanaCarga.getJugadorCargado();
	}

	public VentanaDeCarga getVentanaDeCarga()
	{
		return ventanaCarga;	
	}

	public void cerrarVentanaDeCarga() 
	{
		this.ventanaCarga.dispose();
	}	
}
