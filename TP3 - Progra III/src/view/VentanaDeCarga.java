package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JTextField;

import codigoDeNegocio.Jugador;
import codigoDeNegocio.Pais;
import codigoDeNegocio.Posicion;

public class VentanaDeCarga extends JDialog
{
	private static final long serialVersionUID = 1L;
	private MainApp padre;
	private JButton btnGuardar = new JButton("Guardar");
	
	private JLabel etiquetaNombre = new JLabel("Ingrese nombre y apellido");
	private JTextField campoNombre = new JTextField();
	
	private JLabel etiquetaFaltas = new JLabel("Ingrese cantidad de faltas");
	private JTextField campoFaltas = new JTextField();
	
	private JLabel etiquetaTarjetas = new JLabel("Ingrese cantidad de tarjetas");
	private JTextField campoTarjetas = new JTextField();
	
	private JLabel etiquetaPuntaje = new JLabel("Ingrese su puntaje promedio");
	private JTextField campoPuntaje = new JTextField();
	
	private JLabel etiquetaPosiciones = new JLabel("Elija las posiciones en las que juega");
	
	private JCheckBox PoBox = new JCheckBox("Arquero");
	private JCheckBox LIzqBox = new JCheckBox("Lateral izquierdo");
	private JCheckBox CIzqBox = new JCheckBox("Central izquierdo");
	private JCheckBox CDerBox = new JCheckBox("Central derecho");
	private JCheckBox LDerBox = new JCheckBox("Lateral derecho");
	private JCheckBox VIzqBox = new JCheckBox("Volante izquierdo");
	private JCheckBox VCentralBox = new JCheckBox("Volante central");
	private JCheckBox VDerBox = new JCheckBox("Volante derecho");
	private JCheckBox PIzqBox = new JCheckBox("Puntero Izquierdo");
	private JCheckBox CDelanteroBox = new JCheckBox("Centro delantero");
	private JCheckBox PDerBox = new JCheckBox("Puntero derecho");
	private ArrayList<JCheckBox> boxes = new ArrayList<JCheckBox>();
	
	private JLabel etiquetaPais = new JLabel("Seleccione nacionalidad");
	private JComboBox<Pais> listaPais;
	
	private JLabel etiquetaGoles = new JLabel("Ingrese la cantidad de goles");
	private JTextField campoGoles = new JTextField();
	
	private enum Parser {usarDouble,usarInteger};
	
	public VentanaDeCarga(MainApp padre)
	{
		this.padre = padre;
		this.configurarVentana(padre.getBounds());
		this.configurarComponentes();
	}

	private void configurarVentana(Rectangle bounds) 
	{
		listaPais = new JComboBox<Pais>();
		listaPais.addItem(null);
		for(Pais pais :Pais.values())	
			listaPais.addItem(pais);
		this.setLayout(null);
		this.setLocation(bounds.x, bounds.y);
		this.setSize(300,700);
		this.setResizable(false);
		this.setTitle("Cargar jugador");
		this.setVisible(true);
	}
	
	private void configurarComponentes() 
	{
		this.configurarEtiquetas();
		this.configurarCampos();
		this.configurarChecks();
		
		this.btnGuardar.setBounds((this.getWidth()/4)/2,this.getHeight()-100,this.getWidth()/2+(this.getWidth()/4),50);
		btnGuardar.addActionListener(padre);
		this.add(this.btnGuardar);
		
		this.listaPais.setBounds((this.getWidth()/4)/2,500,this.getWidth()/2+(this.getWidth()/4),20);
		this.listaPais.setBackground(Color.WHITE);
		this.listaPais.setAutoscrolls(true);
		this.add(this.listaPais);
	}

	private void configurarEtiquetas()
	{
		Dimension tamanoEtiqueta = new Dimension(250,20);
		
		this.etiquetaNombre.setSize(tamanoEtiqueta);
		this.etiquetaNombre.setLocation(this.getWidth()/4,10);
		this.add(this.etiquetaNombre);
		
		this.etiquetaFaltas.setSize(tamanoEtiqueta);
		this.etiquetaFaltas.setLocation(this.getWidth()/4,60);
		this.add(this.etiquetaFaltas);
		
		this.etiquetaTarjetas.setSize(tamanoEtiqueta);
		this.etiquetaTarjetas.setLocation(this.getWidth()/4,110);
		this.add(this.etiquetaTarjetas);
		
		this.etiquetaPuntaje.setSize(tamanoEtiqueta);
		this.etiquetaPuntaje.setLocation(this.getWidth()/4,160);
		this.add(this.etiquetaPuntaje);
		
		this.etiquetaPosiciones.setSize(tamanoEtiqueta);
		this.etiquetaPosiciones.setLocation(this.getWidth()/4-20,210);
		this.add(this.etiquetaPosiciones);
		
		this.etiquetaPais.setSize(tamanoEtiqueta);
		this.etiquetaPais.setLocation(this.getWidth()/4,480);
		this.add(this.etiquetaPais);
		
		this.etiquetaGoles.setSize(tamanoEtiqueta);
		this.etiquetaGoles.setLocation(this.getWidth()/4,530);
		this.add(this.etiquetaGoles);
	}
	
	private void configurarCampos() 
	{
		Dimension tamanoCampo = new Dimension(150,20);
		
		this.campoNombre.setLocation(this.getWidth()/4,30);
		this.campoNombre.setSize(tamanoCampo);
		this.add(this.campoNombre);
		
		this.campoFaltas.setLocation(this.getWidth()/4,80);
		this.campoFaltas.setSize(tamanoCampo);
		this.add(this.campoFaltas);
		
		this.campoTarjetas.setLocation(this.getWidth()/4,130);
		this.campoTarjetas.setSize(tamanoCampo);
		this.add(this.campoTarjetas);
		
		this.campoPuntaje.setLocation(this.getWidth()/4,180);
		this.campoPuntaje.setSize(tamanoCampo);
		this.add(this.campoPuntaje);
		
		this.campoGoles.setLocation(this.getWidth()/4,550);
		this.campoGoles.setSize(tamanoCampo);
		this.add(this.campoGoles);
	}
	
	private void configurarChecks() 
	{
		Dimension tamanoCheck = new Dimension(150,20);
		
		this.PoBox.setSize(tamanoCheck);
		this.PoBox.setLocation(this.getWidth()/4, 240);
		this.add(this.PoBox);
		boxes.add(PoBox);
		
		this.LIzqBox.setSize(tamanoCheck);
		this.LIzqBox.setLocation(this.getWidth()/4, 260);
		this.add(this.LIzqBox);
		boxes.add(LIzqBox);
		
		this.CIzqBox.setSize(tamanoCheck);
		this.CIzqBox.setLocation(this.getWidth()/4, 280);
		this.add(this.CIzqBox);
		boxes.add(CIzqBox);
		
		this.CDerBox.setSize(tamanoCheck);
		this.CDerBox.setLocation(this.getWidth()/4, 300);
		this.add(this.CDerBox);
		boxes.add(CDerBox);
		
		this.LDerBox.setSize(tamanoCheck);
		this.LDerBox.setLocation(this.getWidth()/4, 320);
		this.add(this.LDerBox);
		boxes.add(LDerBox);
		
		this.VIzqBox.setSize(tamanoCheck);
		this.VIzqBox.setLocation(this.getWidth()/4, 340);
		this.add(this.VIzqBox);
		boxes.add(VIzqBox);
		
		this.VCentralBox.setSize(tamanoCheck);
		this.VCentralBox.setLocation(this.getWidth()/4, 360);
		this.add(this.VCentralBox);
		boxes.add(VCentralBox);
		
		this.VDerBox.setSize(tamanoCheck);
		this.VDerBox.setLocation(this.getWidth()/4, 380);
		this.add(this.VDerBox);
		boxes.add(VDerBox);
		
		this.PIzqBox.setSize(tamanoCheck);
		this.PIzqBox.setLocation(this.getWidth()/4, 400);
		this.add(this.PIzqBox);
		boxes.add(PIzqBox);
		
		this.CDelanteroBox.setSize(tamanoCheck);
		this.CDelanteroBox.setLocation(this.getWidth()/4, 420);
		this.add(this.CDelanteroBox);
		boxes.add(CDelanteroBox);
		
		this.PDerBox.setSize(tamanoCheck);
		this.PDerBox.setLocation(this.getWidth()/4, 440);
		this.add(this.PDerBox);
		boxes.add(PDerBox);
		
	}

	public Jugador getJugadorCargado() 
	{
		verificarFormularioCompleto();
		return generarJugador();
	}

	private void verificarFormularioCompleto() 
	{
		chequearNombre();
		chequearIntegridad(campoFaltas,"faltas",Parser.usarInteger);
		chequearIntegridad(campoTarjetas,"tarjetas",Parser.usarInteger);
		chequearIntegridad(campoPuntaje,"puntaje promedio",Parser.usarDouble);
		chequearIntegridadPosiciones();
		chequearIntegridadNacionalidad();
		chequearIntegridad(campoGoles,"goles",Parser.usarInteger);
	}

	private void chequearIntegridad(JTextField campo,String valor,Parser parser)
	{
		if (campo.getText().equals(""))
			throw new IllegalArgumentException("Ingrese un valor para cantidad de "+valor+".");
		Pattern pattern = Pattern.compile("[\\d-.]+");
		Matcher matcher = pattern.matcher(campo.getText());
		if (!matcher.matches())
			if (parser.equals(Parser.usarInteger))
				throw new IllegalArgumentException("por favor, utilice solo numeros.");
			else
				throw new IllegalArgumentException("por favor, utilice solo numeros y '.'");
		if (parser.equals(Parser.usarInteger))
			if (Integer.parseInt(campo.getText()) < 0) 
				throw new IllegalArgumentException("La cantidad de "+valor+" debe ser mayor o igual a 0.");
		else
			if (Double.parseDouble(campo.getText()) < 0) 
				throw new IllegalArgumentException("La cantidad de "+valor+" debe ser mayor o igual a 0.");	
	}
	
	private void chequearNombre()
	{
		Pattern pattern = Pattern.compile("[a-zA-Z]+ [a-zA-Z]+");
		Matcher matcher = pattern.matcher(campoNombre.getText());
		
		if (!matcher.matches())
			throw new IllegalArgumentException("El nombre que ingreso es invalido.");
	}

	private void chequearIntegridadPosiciones()
	{
		for (JCheckBox check : boxes)
			if(check.isSelected())
				return;
		throw new IllegalArgumentException("Se debe seleccionar al menos una posicion.");
	}
	
	private void chequearIntegridadNacionalidad()
	{
		if (listaPais.getItemAt(listaPais.getSelectedIndex())==null)
			throw new IllegalArgumentException("Se debe seleccionar una nacionalidad para el jugador.");
	}
	
	private Jugador generarJugador()
	{
		String nombre = campoNombre.getText();
		int faltas = Integer.parseInt(campoFaltas.getText());
		int tarjetas = Integer.parseInt(campoTarjetas.getText());
		double puntaje = Double.parseDouble(campoPuntaje.getText());
		Posicion[] posiciones = damePosicionesSeleccionadas();
		Pais nacionalidad = listaPais.getItemAt(listaPais.getSelectedIndex());
		int goles = Integer.parseInt(campoGoles.getText());
		return new Jugador (nombre,faltas,tarjetas,puntaje,posiciones,nacionalidad,goles);
	}

	private Posicion[] damePosicionesSeleccionadas() 
	{
		ArrayList<Posicion> auxiliar = new ArrayList<Posicion>();
		Posicion[] posiciones = Posicion.values();
		for(int i = 0; i < boxes.size();i++)
			if (boxes.get(i).isSelected())
				auxiliar.add(posiciones[i]);
		Posicion[] ret = new Posicion[auxiliar.size()];
		for (int i = 0; i < ret.length; i++)
			ret[i] = auxiliar.get(i);
		return ret;
	}
}
