package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JTextArea;

import codigoDeNegocio.GeneradorAleatorio;
import codigoDeNegocio.Individuo;
import codigoDeNegocio.Instancia;
import codigoDeNegocio.Jugador;
import codigoDeNegocio.Poblacion;

public class VentanaEquipo extends JDialog implements WindowListener, MouseListener, ActionListener
{
	private static final long serialVersionUID = 1L;
	private PanelLista panelLista;
	private PanelCancha panelCancha;
	private MainApp padre;
	private JButton btnAgregarJugador;
	private JButton btnGenerarMejorEquipo;
	private Jugador jugadorSeleccionado;
	private JTextArea fitnessDisplay;
	private boolean isVentanaJugadorActive;

	public VentanaEquipo(MainApp padre)
	{
		this.padre = padre;
		configurarDialog();
		configurarComponentes();
		configurarBotones();
	}

	private void configurarDialog() 
	{
		this.configurarDimensiones();
		this.addWindowListener(this);
		this.setLayout(null);
		this.setResizable(false);
		this.getContentPane().setBackground(new Color(86, 7, 12));
		this.setTitle("Editor de equipo");
		this.setVisible(true);
	}
	
	private void configurarDimensiones() 
	{
		Toolkit miPantalla = Toolkit.getDefaultToolkit();
		Dimension resolucion = miPantalla.getScreenSize();
		
		int anchoPantalla = (int) resolucion.getWidth();
		int altoPantalla = (int) resolucion.getHeight();
		
		this.setSize(anchoPantalla/2, altoPantalla/2);
		this.setLocation(anchoPantalla/4, altoPantalla/4);
	}
	private void configurarComponentes()
	{
		this.panelLista = new PanelLista(this);
		this.getContentPane().add(panelLista);
		
		this.panelCancha = new PanelCancha(this);
		this.getContentPane().add(panelCancha);
	}

	private void configurarBotones()
	{
		this.btnAgregarJugador = new JButton("Agregar Jugador");
		btnAgregarJugador.addActionListener(padre);
		this.btnAgregarJugador.setSize(133, 30);
		this.btnAgregarJugador.setLocation(10,this.getHeight()-70);
		this.getContentPane().add(btnAgregarJugador);
		this.btnGenerarMejorEquipo = new JButton("Generar Mejor Equipo");
		btnGenerarMejorEquipo.addActionListener(padre);
		this.btnGenerarMejorEquipo.setSize(170, 30);
		this.btnGenerarMejorEquipo.setLocation(btnAgregarJugador.getWidth()/2+btnGenerarMejorEquipo.getWidth()/2,this.getHeight()-70);
		this.getContentPane().add(btnGenerarMejorEquipo);
		this.fitnessDisplay = new JTextArea();
		fitnessDisplay.setEditable(false);
		fitnessDisplay.setLocation(btnGenerarMejorEquipo.getX()+180, this.getHeight()-65);
		fitnessDisplay.setSize(170,16);
		fitnessDisplay.setBackground(null);
		fitnessDisplay.setText("Fitness Actual:");
		Font f = new Font(null,Font.LAYOUT_RIGHT_TO_LEFT,14);
		fitnessDisplay.setForeground(new Color(212, 175, 55));
		fitnessDisplay.setFont(f);
		this.add(fitnessDisplay);
		
		this.repaint();
	}
	
	@Override
	public void windowActivated(WindowEvent e) 
	{
		
	}

	@Override
	public void windowClosed(WindowEvent e) 
	{
	}

	@Override
	public void windowClosing(WindowEvent e) 
	{
		if (this.isVentanaJugadorActive)
		{
			this.padre.getVentanaJugador().setVisible(false);
			this.isVentanaJugadorActive = false;
		}
		else
		{
			
			this.padre.setVisible(true);
		}
	}

	@Override
	public void windowDeactivated(WindowEvent e) 
	{
		
	}

	@Override
	public void windowDeiconified(WindowEvent e) 
	{
		
	}

	@Override
	public void windowIconified(WindowEvent e) 
	{
		
	}

	@Override
	public void windowOpened(WindowEvent e) 
	{
		
	}
	
	@Override
	public void mouseClicked(MouseEvent e) 
	{
	}

	@Override
	public void mouseEntered(MouseEvent e) 
	{
		
	}

	@Override
	public void mouseExited(MouseEvent e) 
	{
		
	}

	@Override
	public void mousePressed(MouseEvent e)
	{
		
	}

	@Override
	public void mouseReleased(MouseEvent e) 
	{
		
	}

	public MainApp getPadre()
	{
		return this.padre;
	}

	public PanelLista getPanelLista()
	{
		return panelLista;
	}

	public void agregarJugadorALista(Jugador jugador) 
	{
		this.panelLista.agregarJugador(jugador);		
	}
	
	public Jugador getJugadorSeleccionado()
	{
		return jugadorSeleccionado;
	}

	public void setJugadorSeleccionado(Jugador jugadorSeleccionado)
	{
		this.jugadorSeleccionado = jugadorSeleccionado;
	}

	@Override
	public void actionPerformed(ActionEvent e)
	{
		padre.actionPerformed(e);
	}

	public PanelCancha getPanelCancha()
	{
		return this.panelCancha;
	}

	public Individuo getEquipo()
	{
		return this.panelCancha.getEquipo();
	}

	public void setValorFitness(double fitnessActual)
	{
		this.fitnessDisplay.setText("Fitness Actual:"+fitnessActual);
	}

	public void setVentanaJugadorActive(boolean b)
	{
		this.isVentanaJugadorActive = b;	
	}

	public void generarMejorEquipo(Instancia instancia)
	{
		Poblacion.setGenerador(new GeneradorAleatorio());
		Individuo.setGenerador(new GeneradorAleatorio());
		Poblacion poblacion = new Poblacion(instancia);
		Individuo mejor;
		try
		{
			mejor = poblacion.simular();
			mejor.ordenarPorPosicion();
			for (int i = 0; i < 11; i++)
				System.out.println(mejor.getJugadorEnPosicion(i).getNombre());
			getPanelCancha().setEquipo(mejor);
			actualizarVisualDeEquipo();
			getPanelLista().setLista(mejor);
		}
		catch (CloneNotSupportedException e1)
		{
			e1.printStackTrace();
		}
	}
	public void actualizarVisualDeEquipo()
	{
		double fitnessActual = getEquipo().fitness();
		setValorFitness(fitnessActual);
		paintComponents(getGraphics());
	}
}
