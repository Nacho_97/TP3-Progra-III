package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JTextField;

import codigoDeNegocio.Jugador;

public class VentanaJugador extends JDialog
{
	private static final long serialVersionUID = 1L;
	private VentanaEquipo padre;
	private JButton btnBuscar;
	private JButton btnAgregarALista;
	private JTextField JTxtInputJugador;
	private PanelEstadisticas panelEstadisticas;
	
	public VentanaJugador(VentanaEquipo padre)
	{
		this.padre = padre;
		configurarDialog();
		configurarComponentes();
	}
	
	private void configurarDialog() 
	{
		this.configurarDimensiones();
		this.addWindowListener(padre);
		this.setLayout(null);
		this.setResizable(false);
		this.getContentPane().setBackground(new Color(86, 7, 12));
		this.setTitle("Agregar Jugador a la Lista");
		this.setVisible(true);
		
		this.panelEstadisticas = new PanelEstadisticas(10,100,this.getWidth()-30,this.getHeight()-180,13);
		this.getContentPane().add(panelEstadisticas);
	}
	
	private void configurarDimensiones() 
	{
		Toolkit miPantalla = Toolkit.getDefaultToolkit();
		Dimension resolucion = miPantalla.getScreenSize();
		
		int anchoPantalla = (int) resolucion.getWidth();
		int altoPantalla = (int) resolucion.getHeight();
		
		this.setSize(250,600);
		this.setLocation(anchoPantalla/4, altoPantalla/4);
	}
	
	private void configurarComponentes()
	{
		btnBuscar = new JButton("Buscar");
		btnBuscar.addActionListener(padre.getPadre());
		btnBuscar.setSize(130, 30);
		btnBuscar.setLocation((this.getWidth()/2)-65,60);
		getContentPane().add(btnBuscar);
		
		btnAgregarALista = new JButton("Agregar a la lista");
		btnAgregarALista.addActionListener(padre.getPadre());
		btnAgregarALista.setSize(160, 30);
		btnAgregarALista.setLocation((this.getWidth()/2)-80,this.getHeight()-70);
		btnAgregarALista.setEnabled(false);
		getContentPane().add(btnAgregarALista);
		
		JLabel label = new JLabel("Ingrese el nombre del Jugador");
		label.setForeground(new Color(212, 175, 55));
		label.setSize(190,20);
		label.setLocation((this.getWidth()/2)-95,0);
		getContentPane().add(label);
		
		JTxtInputJugador = new JTextField();
		JTxtInputJugador.setSize(190,20);
		JTxtInputJugador.setLocation((this.getWidth()/2)-95,30);
		getContentPane().add(JTxtInputJugador);
	}

	public void setAgregarAListaActive(boolean b)
	{
		btnAgregarALista.setEnabled(b);
	}

	public JTextField getTextoABuscar()
	{
		return this.JTxtInputJugador;
	}

	public String getJugadorABuscar()
	{
		return this.JTxtInputJugador.getText();
	}

	public void mostrarJugador(Jugador jugador) 
	{
		this.panelEstadisticas.mostrarDatosJugador(jugador);
	}
}
